/*
 * basic_Shell.c
 *
 *  Created on: 2014-07-28
 *      Author: Adrien Lapointe
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>

const unsigned int cmd_max_len = 1024;
const unsigned int max_num_arg = 64;
#define path_max 255

void welcome_prompt();
void parse(char *input, char **argv);
void type_prompt(char *input);
void interpret_command(char **argv);
void change_directory(char* dest);
void list_sub();
void print_work_dir();

int main() {

	char input[cmd_max_len];             // the input
	char *argv[max_num_arg];              // the arguments

	//int pid, status;
	int i = 0;
	//int idx = 1;


	welcome_prompt();

	while (1) {
		for (i = 0; i< max_num_arg; i++){
			argv[i] = NULL;
		}
		type_prompt(input);
		//pid = waitpid(-1, &status, WNOHANG); // This is necessary for the fork.
		parse(input, argv);
		interpret_command(argv);

	}

	return 0;
}

/*
 * This functions prints the prompt message and accepts input from the user.
 */
void type_prompt(char *input) {
	fflush(stdout);
	printf("GEF435$");

	if (input != NULL) {
		int c = EOF;
		int i = 0;

		// take in input until user hits enter or end of file is encountered.
		while ((c = getchar()) != '\n' && c != EOF) {
			input[i++] = (char) c;
		}

		input[i] = '\0';
	}
}

/*
 * This function parses the user inputs.
 */
void parse(char *input, char **argv) {
	   char *token;
	   int idx = 0;
	   // Get First argument (i.e. program name)
	   token = strtok(input, " ");

	   // Store arguments to argv, advance to next argument
	   while( token != NULL ) {
	      argv[idx] = token;
	      idx++;
	      token = strtok(NULL, " ");
	   }

	   return;
}

/*
 * This function interprets the parsed command that is entered by the user and
 * calls the appropriate built-in function or calls the appropriate program.
 */
void interpret_command(char **argv) {
	char* command = argv[0];
	int status;
	//build list of arguments
//	char *arguments[max_num_arg - 1];
//	int idx = 1;
//	int i = 0;
//	for (i = 0; i < (max_num_arg - 1); i++){
//			arguments[i] = NULL;
//		}
//	i = 0;
//	while (argv[idx] != NULL){
//		arguments[i] = argv[idx];
//		i++;
//		idx++;
//	}

	//check for built ins, call built in functions if necessary
	if (strcmp(command, "exit") == 0){
		exit(0);
	}
	else if (strcmp(command, "cd") == 0){
		change_directory(argv[1]);
		}
	else if (strcmp(command, "ls") == 0){
		list_sub();
		}
	else if (strcmp(command, "pwd") == 0){
		print_work_dir();
		}
	else{

		// if process is the child
		if (fork() == 0){
			//create a filepath to find the command
			char filepath[path_max] = "/usr/bin/";
			strcat(filepath, command);
			//change to this program
			execv(filepath, argv);
			//error message if program change fails
			printf("No such program in /usr/bin\n");
			exit(1);
		}else {
			// if parent process, wait out
			wait(&status);
		}
	}


}

/*
 * This function prints the welcome message.
 */
void welcome_prompt() {
	int num_Padding = 41;
	int i;

// Prints the first line of padding.
	for (i = 0; i < num_Padding; i++)
		printf("#");
	printf("\n#\tWelcome to the GEF435 shell\t#\n");
	for (i = 0; i < num_Padding; i++)
		printf("#");
	printf("\n\n");
}

//changes directories to the destination
void change_directory(char* dest) {
	chdir(dest);
}

//lists contents of the current directory
void list_sub() {
	char work_dir[path_max];
	struct dirent *dir;
	DIR *d;

	getcwd(work_dir, path_max);
	//open current directory
	d = opendir(work_dir);
	//use readdir to read from the current directory stream
	dir = readdir(d);
	while (dir != NULL){
		//print the contents, move on to the next item
		printf("%s\n", dir->d_name);
		dir = readdir(d);
	}
	closedir(d);
}

//prints the working directory
void print_work_dir(){
	char work_dir[path_max];
	getcwd(work_dir, path_max);
	printf("%s\n", work_dir);
}
